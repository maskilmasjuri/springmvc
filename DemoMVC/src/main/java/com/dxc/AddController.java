package com.dxc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dxc.service.AddService;

@Controller
public class AddController {
	@RequestMapping("/add")
//	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
	public ModelAndView add(@RequestParam("t1") int num1, @RequestParam("t2") int num2, HttpServletRequest request, HttpServletResponse response) {
//		int num1 = Integer.parseInt(request.getParameter("t1"));
//		int num2 = Integer.parseInt(request.getParameter("t2"));
//		int result = num1 + num2;
		
		AddService addService = new AddService();
		int result = addService.add(num1, num2);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("display");
		mv.addObject("result", result);
		
		return mv;
	}
}
